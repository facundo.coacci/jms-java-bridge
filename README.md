# jms-java-bridge

Projecto que contiene el bridge de java para integrar MAT con plataformas JMS 

## Jython container

Se desarrollo un Dockerfile para generar un container de jython

Para PRODUCCION mejor basarse en openjdk:jre-buster o openjdk:jre-alpine

para DESARROLLO mejor utilizar openjdk:8-jdk-buster o similar. La razon es porque permite compilar codigo Java para hacer pruebas.

Se monta un Path en CLASSPATH=/home/jars/* para poder montar un volumen y agregar jars al momento del desarrollo.

`docker run -it -v jars/:/home/jars/ jython:8-jdk-buster jython`


## Weblogic Adapter

JMSProducer es una clase que implementa un productor de mensajes.

```

from JMSProducer import JMSProducer

jms = JMSProducer()
jms.init_conn_params()

for i in range(10000):
    jms.send("JMS Queue testing {}".format(i))

jms.close_conn_params()


```

JMSConsumer es una clase que implementa un consumidor de mensajes

```

from JMSConsumer import JMSConsumer

consumer = JMSConsumer()
consumer.initialize_conn_params()
while True:
    msg = consumer.queue_receiver.receive()
    print(msg.getText())

```



  
## IBM Adapter [WIP]

MQProducer es una clase que implementa un productor de mensajes.

```

from MQProducer import MQProducer

mq = MQProducer()
mq.init_conn_params()

for i in range(10000):
    mq.send("MQ Queue testing {}".format(i))

mq.close_conn_params()


```


## Ambiente de testing

Se puede levantar un JMS Weblogic y un IBM MQ con docker.

IBM Viene con una cola de desarrollo preconfigurada: 

Imagen: https://developer.ibm.com/tutorials/mq-connect-app-queue-manager-containers/

Weblogic hay que configurarla a mano, se puede seguir este intructivo: 

Imagen: https://hub.docker.com/r/ismaleiva90/weblogic12/
Como levantar la Queue: https://www.benchresources.net/oracle-weblogic-steps-to-create-connection-factory-and-queue/










