import java.util.Hashtable;
import javax.jms.JMSException as JMSException
import javax.jms.Message as Message
import javax.jms.MessageListener as MessageListener
import javax.jms.Queue as Queue
import javax.jms.QueueConnection as QueueConnection
import javax.jms.QueueConnectionFactory as QueueConnectionFactory
import javax.jms.QueueReceiver as QueueReceiver
import javax.jms.QueueSession as QueueSession
import javax.jms.Session as Session
import javax.jms.TextMessage as TextMessage
import javax.naming.Context as context
import javax.naming.InitialContext as InitialContext
import javax.naming.NamingException

#import json
 
class JMSConsumer(javax.jms.MessageListener):
    
    ## JNDI name for Weblogic, ConnectionFactory & Queue
    WEBLOGIC_JMS_URL = "t3://172.17.0.3:7001" # Weblogic JMS URL
    WEBLOGIC_JNDI_FACTORY_NAME = "weblogic.jndi.WLInitialContextFactory" # Weblogic JNDI
    CONNECTION_FACTORY_JNDI_NAME = "jms/test/TestConnFactory" # Weblogic ConnectionFactory JNDI
    QUEUE_JNDI_NAME = "jms/test/TestQueue" # Weblogic Queue JNDI
    
    # variables 
    env = None
    initial_context = None
    queue_conn_factory = None   
    queue_conn = None
    queue = None
    queue_receiver = None
    text_message = None
    quit = None
     
    messages = []


    def __init__(self, jms_url, jndi_factory, jndi_conn_factory, jndi_queue, user='weblogic', password='welcome1'):
        """
        Args:
           jms_url: "t3://localhost:7001"
           jndi_factory: "weblogic.jndi.WLInitialContextFactory"
           jndi_conn_factory: "jms/test/TestConnFactory"
           jndi_queue: "jms/test/TestQueue"
        """
        
        self.WEBLOGIC_JMS_URL = jms_url
        self.WEBLOGIC_JNDI_FACTORY_NAME = jndi_factory
        self.CONNECTION_FACTORY_JNDI_NAME = jndi_conn_factory
        self.QUEUE_JNDI_NAME = jndi_queue

        self.env = java.util.Hashtable()
        self.env.put(context.PROVIDER_URL, self.WEBLOGIC_JMS_URL)
        self.env.put(context.INITIAL_CONTEXT_FACTORY, self.WEBLOGIC_JNDI_FACTORY_NAME)
        self.env.put(context.SECURITY_PRINCIPAL, user)
        self.env.put(context.SECURITY_CREDENTIALS, password)

    def initialize_conn_params(self, message_selector=None): 
 
        self.initial_context = InitialContext(self.env) 
        self.queue_conn_factory = self.initial_context.lookup(self.CONNECTION_FACTORY_JNDI_NAME)
        self.queue_conn = self.queue_conn_factory.createQueueConnection()
        self.queue_session = self.queue_conn.createQueueSession(False, Session.AUTO_ACKNOWLEDGE)
        self.queue = self.initial_context.lookup(self.QUEUE_JNDI_NAME)
        self.queue_receiver = self.queue_session.createReceiver(self.queue, message_selector)
        # self.queue_receiver.setMessageListener(self)
        self.queue_conn.start()


if __name__ == "__main__":
    
    consumer = JMSConsumer(
        jms_url="t3://172.17.0.3:7001",
        jndi_factory= "weblogic.jndi.WLInitialContextFactory",
        jndi_conn_factory= "jms/test/TestConnFactory",
        jndi_queue= "jms/test/TestQueue",
        user="weblogic",
        password="welcome1"
    )

    print(consumer.env)
    
    consumer.initialize_conn_params(message_selector="header1 = 'otroValor'")
    while True:
        msg = consumer.queue_receiver.receive()
        
        # Do something with message.
        print(msg.getText())
        print(msg.getStringProperty('header1'))


        