import java.io.IOException as IOException
import java.io.OutputStream as OutputStream 
import java.net.InetSocketAddress as InetSocketAddress 

import com.sun.net.httpserver.HttpExchange as HttpExchange
import com.sun.net.httpserver.HttpHandler as HttpHandler
import com.sun.net.httpserver.HttpServer as HttpServer

class Handler(HttpHandler):

    def handle(t):
        response = "This is the response"
        t.sendResponseHeaders(200, response.length())
        os = t.getResponseBody()
        os.write(response.getBytes())
        os.close()

if __name__ == "__main__":
    server = HttpServer.create(InetSocketAddress(8000), 0)
    server.createContext("/test", Handler())
    server.setExecutor(None)
    server.start()