from wsgiref import headers
import java.util.Hashtable as Hashtable
 
import javax.jms.JMSException as JMSException
import javax.jms.Queue as Queue
import javax.jms.QueueConnection as QueueConnection
import javax.jms.QueueConnectionFactory as QueueConnectionFactory
import javax.jms.QueueSender as QueueSender
import javax.jms.QueueSession as QueueSession
import javax.jms.Session as Session
import javax.jms.TextMessage as TextMessage
import javax.naming.Context as Context
import javax.naming.InitialContext as InitialContext
import javax.naming.NamingException as NamingException
 
class JMSProducer:
 
    #// JNDI name for Weblogic, ConnectionFactory & Queue
    WEBLOGIC_JMS_URL = "t3://172.17.0.3:7001"
    WEBLOGIC_JNDI_FACTORY_NAME = "weblogic.jndi.WLInitialContextFactory" 
    CONNECTION_FACTORY_JNDI_NAME = "jms/test/TestConnFactory" 
    QUEUE_JNDI_NAME = "jms/test/TestQueue" 
 
    #// variables 
    env = None
    initial_context = None
    queue_conn_factory = None
    queue_conn = None
    queue_session = None
    queue = None
    queue_sender = None
    text_message = None
 
    def __init__(self, properties_file=None):
 
        self.env = Hashtable()
        self.env.put(Context.PROVIDER_URL, self.WEBLOGIC_JMS_URL)
        self.env.put(Context.INITIAL_CONTEXT_FACTORY, self.WEBLOGIC_JNDI_FACTORY_NAME)
        self.env.put(Context.SECURITY_PRINCIPAL, "weblogic")
        self.env.put(Context.SECURITY_CREDENTIALS, "welcome1")
    
    def init_conn_params(self):
 
        self.initial_context = InitialContext(self.env)
        self.queue_conn_factory = self.initial_context.lookup(self.CONNECTION_FACTORY_JNDI_NAME)
        self.queue_conn = self.queue_conn_factory.createQueueConnection()
        self.queue_session = self.queue_conn.createQueueSession(False, Session.AUTO_ACKNOWLEDGE)
        self.queue = self.initial_context.lookup(self.QUEUE_JNDI_NAME)
        self.queue_sender = self.queue_session.createSender(self.queue)
        self.text_message = self.queue_session.createTextMessage()
        self.queue_conn.start()
    
 
    def send(self, message, headers={}):
        for key in headers:
            self.text_message.setStringProperty(key, headers[key])

        self.text_message.setText(message)
        self.queue_sender.send(self.text_message)
 

    def close_conn_params(self):
 
        self.queue_sender.close()
        self.queue_session.close()
        self.queue_conn.close()
    
 

if __name__ == "__main__":

    jms = JMSProducer()
    jms.init_conn_params()
    headers ={
        'header1': 'value1',
        'header2': 'value2'
    }
    
    for i in range(10):
        if i % 2:
            headers['header1'] = 'otroValor'    
        jms.send("JMS Queue testing {}".format(i), headers=headers)
        
    jms.close_conn_params()
    
        
