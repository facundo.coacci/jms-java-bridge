import java.util.Hashtable;
 
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
 

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import java.util.ArrayList;


public class JMSQueueConsumer implements MessageListener {
 
    // JNDI name for Weblogic, ConnectionFactory & Queue
    private String WEBLOGIC_JMS_URL = "t3://172.17.0.2:7001"; // Weblogic JMS URL
    public  String WEBLOGIC_JNDI_FACTORY_NAME = "weblogic.jndi.WLInitialContextFactory"; // Weblogic JNDI
    private String CONNECTION_FACTORY_JNDI_NAME = "jms/test/TestConnFactory"; // Weblogic ConnectionFactory JNDI
    private String QUEUE_JNDI_NAME = "jms/test/TestQueue"; // Weblogic Queue JNDI
    private String WEBHOOK_URL = "https://httpbin.org/anything"; // Weblogic Queue JNDI
 
    // variables 
    private Hashtable<String, String> wlsEnvParamHashTbl = null;
    private static InitialContext initialContext = null;
    private static QueueConnectionFactory queueConnectionFactory = null;
    private static QueueConnection queueConnection = null;
    private static QueueSession queueSession = null;
    private static Queue queue = null;
    private static QueueReceiver queueReceiver = null;
    private static TextMessage textMessage = null;
    private boolean quit = false;
 
    /**
     * default constructor
     */
    public JMSQueueConsumer( String user, String password) {
 
        wlsEnvParamHashTbl = new Hashtable<String, String>();
        wlsEnvParamHashTbl.put(Context.PROVIDER_URL, WEBLOGIC_JMS_URL); // set Weblogic JMS URL
        wlsEnvParamHashTbl.put(Context.INITIAL_CONTEXT_FACTORY, WEBLOGIC_JNDI_FACTORY_NAME); // set Weblogic JNDI
        wlsEnvParamHashTbl.put(Context.SECURITY_PRINCIPAL, user); // set Weblogic UserName
        wlsEnvParamHashTbl.put(Context.SECURITY_CREDENTIALS, password); // set Weblogic PassWord
    }

    public JMSQueueConsumer(String providerUrl, String jndiFactoryName, String jndiConnFactoryName, String queueName, String user, String password, String hookUrl) {
        
        this.CONNECTION_FACTORY_JNDI_NAME = jndiConnFactoryName;
        this.WEBLOGIC_JMS_URL = providerUrl;
        this.WEBLOGIC_JNDI_FACTORY_NAME = jndiFactoryName;
        this.QUEUE_JNDI_NAME = queueName;
        this.WEBHOOK_URL = hookUrl;

        wlsEnvParamHashTbl = new Hashtable<String, String>();
        wlsEnvParamHashTbl.put(Context.PROVIDER_URL, WEBLOGIC_JMS_URL); // set Weblogic JMS URL
        wlsEnvParamHashTbl.put(Context.INITIAL_CONTEXT_FACTORY, WEBLOGIC_JNDI_FACTORY_NAME); // set Weblogic JNDI
        wlsEnvParamHashTbl.put(Context.SECURITY_PRINCIPAL, user); // set Weblogic UserName
        wlsEnvParamHashTbl.put(Context.SECURITY_CREDENTIALS, password); // set Weblogic PassWord
    }    

    /**
     * This method initializes all necessary connection parameters for establishing JMS Queue communication
     * 
     * @throws NamingException
     * @throws JMSException
     */
    public void initializeConnParams() throws NamingException, JMSException {
 
        initialContext = new InitialContext(wlsEnvParamHashTbl); // set InitialContext 
        queueConnectionFactory = (QueueConnectionFactory) initialContext.lookup(CONNECTION_FACTORY_JNDI_NAME); // lookup using initial context
        queueConnection = queueConnectionFactory.createQueueConnection(); // create ConnectionFactory
        queueSession = queueConnection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE); // create QueueSession
        queue = (Queue) initialContext.lookup(QUEUE_JNDI_NAME); // lookup Queue JNDI using initial context created above
        queueReceiver = queueSession.createReceiver(queue); // create Receiver using Queue JNDI as arguments
        queueReceiver.setMessageListener(this); // set Message Listener
        queueConnection.start(); // start Queue connection
    }
 
    /**
     * onMessage() listener from MessageListener class to read messages
     */
    @Override
    public void onMessage(Message message) {
 
        String consumedMessagefromQueue = null;
 
        try {
            if (message instanceof TextMessage) { // It's TextMessage...
 
                textMessage = (TextMessage) message;
                consumedMessagefromQueue = textMessage.getText();
            }
            else { // If it is not a TextMessage...
 
                consumedMessagefromQueue = message.toString();
            }
 
            // finally print the message to the output console
            System.out.println("JMSQueueConsumer: Message consumed from JMS Queue 'TestQueue' is >>>" + consumedMessagefromQueue + "<<<");
            
            this.sendPOST(WEBHOOK_URL, consumedMessagefromQueue);

            if (consumedMessagefromQueue.equalsIgnoreCase("quit")) {
                synchronized(this) {
                    quit = true;
                    this.notifyAll(); // Notify main thread to quit
                }
            }
        } 
        catch (JMSException jmsex) {
            jmsex.printStackTrace();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
 
    /**
     * This method closes all connections
     * 
     * @throws JMSException
     */
    public void close() throws JMSException {
 
        queueReceiver.close();
        queueSession.close();
        queueConnection.close();
    }
 
    /**
     * This method do HTTP POST to an URI
     * 
     * @throws Exception
     */
    private String sendPOST(String url, String json) throws Exception {
    
        URL UrlObj = new URL(url);
    
        HttpURLConnection connection = (HttpURLConnection) UrlObj.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0");
        connection.setRequestProperty("Content-Type", "application/json; utf-8");
        connection.setRequestProperty("Accept", "application/json");
        connection.setDoOutput(true);
    
        // String jsonInputString = "{\"name\": \"Upendra\", \"job\": \"Programmer\"}";

        DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
    
        byte[] input = json.getBytes("utf-8");
        outputStream.write(input, 0, input.length);
        outputStream.flush();
        outputStream.close();
    
        System.out.println("Send 'HTTP POST' request to : " + url);
    
        int responseCode = connection.getResponseCode();
        System.out.println("Response Code : " + responseCode);
    
        if (responseCode == HttpURLConnection.HTTP_OK) {
            BufferedReader inputReader = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
    
            while ((inputLine = inputReader.readLine()) != null) {
                response.append(inputLine);
            }
            inputReader.close();

            System.out.println(response.toString());
            return response.toString();
        }

        return null;
    }



    /**
     * This is main() method to initiate all necessary calls
     * 
     * @param args
     */
    public static void main(String[] args) throws Exception {
 
        InputStream input = new FileInputStream("weblogicServiceConsumer.properties");

        Properties prop = new Properties();

        // load a properties file
        prop.load(input);

        // get the property value and print it out
        System.out.println(prop.getProperty("weblogic.url"));
        System.out.println(prop.getProperty("weblogic.factory"));
        System.out.println(prop.getProperty("weblogic.connection"));
        System.out.println(prop.getProperty("weblogic.queue"));
        // System.out.println(prop.getProperty("weblogic.user"));
        //System.out.println(prop.getProperty("weblogic.password"));
        System.out.println(prop.getProperty("weblogic.webhook"));
        String[] queues = prop.getProperty("weblogic.queues").split(",");
        

        ArrayList<JMSQueueConsumer> jmsQueues = new ArrayList<JMSQueueConsumer>(); 

        for (String queue: queues) {
            System.out.println(queue);

            // create an object to invoke initialize(), onMessage() and close() methods
            JMSQueueConsumer jmsQueue = new JMSQueueConsumer(
                prop.getProperty("weblogic.url"),
                prop.getProperty("weblogic.factory"),
                prop.getProperty("weblogic.connection"),
                // prop.getProperty("weblogic.queue"),
                queue,
                prop.getProperty("weblogic.user"),
                prop.getProperty("weblogic.password"),
                prop.getProperty("weblogic.webhook")
            );

            jmsQueue.initializeConnParams();
            
            jmsQueues.add(jmsQueue);

        }
            
        while (true){
            int a = 1;
        }
 
       /* try {
            // to invoke initialize() method to establish all necessary connections
            //jmsQueueConsumer.initializeConnParams();
 
            / 
            synchronized(jmsQueueConsumer) {
                while (! jmsQueueConsumer.quit) {
                    try {
                        jmsQueueConsumer.wait();
                    } catch (InterruptedException ie) {}
                }
            }
 
            // to invoke close() method to close all established connections
            while (true){
                int a = 1;
            }
            // jmsQueueConsumer.close();
        } 
        catch (NamingException nex) {
            nex.printStackTrace();
        } 
        catch (JMSException jmsex) {
            jmsex.printStackTrace();
        } */
    }
}

