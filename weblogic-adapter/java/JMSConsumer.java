import java.util.Hashtable;
 
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


 
public class JMSConsumer implements MessageListener {
 
    // JNDI name for Weblogic, ConnectionFactory, Queue and webhook properties 
    private String WEBLOGIC_JMS_URL = "t3://172.17.0.3:7001"; // Weblogic JMS URL
    private String WEBLOGIC_JNDI_FACTORY_NAME = "weblogic.jndi.WLInitialContextFactory"; // Weblogic JNDI
    private String CONNECTION_FACTORY_JNDI_NAME = "jms/test/TestConnFactory"; // Weblogic ConnectionFactory JNDI
    private String QUEUE_JNDI_NAME = "jms/test/TestQueue"; // Weblogic Queue JNDI
    private String WEBHOOK_URL = "https://httpbin.org/anything";

    // variables 
    private Hashtable<String, String> env = null;
    private InitialContext initialContext = null;
    private QueueConnectionFactory queueConnectionFactory = null;
    private QueueConnection queueConnection = null;
    private QueueSession queueSession = null;
    private Queue queue = null;
    private QueueReceiver queueReceiver = null;
    private TextMessage textMessage = null;
    private boolean quit = false;


    /**
     * default constructor
     */
    public JMSConsumer() {
 
        env = new Hashtable<String, String>();
        env.put(Context.PROVIDER_URL, WEBLOGIC_JMS_URL); // set Weblogic JMS URL
        env.put(Context.INITIAL_CONTEXT_FACTORY, WEBLOGIC_JNDI_FACTORY_NAME); // set Weblogic JNDI
        env.put(Context.SECURITY_PRINCIPAL, "weblogic"); // set Weblogic UserName
        env.put(Context.SECURITY_CREDENTIALS, "welcome1"); // set Weblogic PassWord
    }
 
    /**
     * This method initializes all necessary connection parameters for establishing JMS Queue communication
     * 
     * @throws NamingException
     * @throws JMSException
     */
    public void initializeConnParams() throws NamingException, JMSException {
 
        initialContext = new InitialContext(env); // set InitialContext 
        queueConnectionFactory = (QueueConnectionFactory) initialContext.lookup(CONNECTION_FACTORY_JNDI_NAME); // lookup using initial context
        queueConnection = queueConnectionFactory.createQueueConnection(); // create ConnectionFactory
        queueSession = queueConnection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE); // create QueueSession
        queue = (Queue) initialContext.lookup(QUEUE_JNDI_NAME); // lookup Queue JNDI using initial context created above
        queueReceiver = queueSession.createReceiver(queue); // create Receiver using Queue JNDI as arguments
        queueReceiver.setMessageListener(this); // set Message Listener
        queueConnection.start(); // start Queue connection
    }
 
    /**
     * onMessage() listener from MessageListener class to read messages
     */
    @Override
    public void onMessage(Message message) {
 
        String consumedMessagefromQueue = null;
 
        try {
            if (message instanceof TextMessage) { // It's TextMessage...
 
                textMessage = (TextMessage) message;
                consumedMessagefromQueue = textMessage.getText();
            }
            else { // If it is not a TextMessage...
 
                consumedMessagefromQueue = message.toString();
            }
 
            // finally print the message to the output console
            System.out.println("JMSQueueConsumer: Message consumed from JMS Queue 'TestQueue' is >>>" + consumedMessagefromQueue + "<<<");


            String response = this.sendPOST(this.WEBHOOK_URL, consumedMessagefromQueue);
            System.out.println(response);
        } 
        
        catch (JMSException jmsex) {
            jmsex.printStackTrace();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * This method do HTTP POST to an URI
     * 
     * @throws Exception
     */
    private static String sendPOST(String url, String json) throws Exception {
    
        URL UrlObj = new URL(url);
    
        HttpURLConnection connection = (HttpURLConnection) UrlObj.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0");
        connection.setRequestProperty("Content-Type", "application/json; utf-8");
        connection.setRequestProperty("Accept", "application/json");
        connection.setDoOutput(true);
    
        String jsonInputString = "{\"name\": \"Upendra\", \"job\": \"Programmer\"}";

        DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
    
        byte[] input = jsonInputString.getBytes("utf-8");
        outputStream.write(input, 0, input.length);
        outputStream.flush();
        outputStream.close();
    
        System.out.println("Send 'HTTP POST' request to : " + url);
    
        int responseCode = connection.getResponseCode();
        System.out.println("Response Code : " + responseCode);
    
        if (responseCode == HttpURLConnection.HTTP_OK) {
            BufferedReader inputReader = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
    
            while ((inputLine = inputReader.readLine()) != null) {
                response.append(inputLine);
            }
            inputReader.close();

            System.out.println(response.toString());
            return response.toString();
        }

        return null;
    }
    /**
     * This method closes all connections
     * 
     * @throws JMSException
     */
    public void close() throws JMSException {
 
        queueReceiver.close();
        queueSession.close();
        queueConnection.close();
    }
 
    /**
     * This is main() method to initiate all necessary calls
     * 
     * @param args
     */
    public static void main(String[] args)  throws Exception {
 
        // create an object to invoke initialize(), onMessage() and close() methods
        JMSConsumer jms = new JMSConsumer();
 
        try {
            // to invoke initialize() method to establish all necessary connections
            jms.initializeConnParams();
 
            // to invoke close() method to close all established connections
            jms.close();
        } 
        catch (NamingException nex) {
            nex.printStackTrace();
        } 
        catch (JMSException jmsex) {
            jmsex.printStackTrace();
        }
    }
        // Jython Implementation to create an object to invoke initialize(), onMessage() and close() methods
        // import JMSQueueConsumer
        // jms = JMSQueueConsumer()
        // jms.initializeConnParams()
        // while not jms.quit: 
        //     jmsQueueConsumer.wait();
        // jmsQueueConsumer.close();
        // javac -cp ./com.ibm.mq.allclient-9.2.4.0.jar:./javax.jms-api-2.0.1.jar:./java-json.jar JMSConsumer.java

}
