import java.util.Hashtable;
 
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
 
public class JMSQueueProducer {
 
    // JNDI name for Weblogic, ConnectionFactory & Queue
    private String WEBLOGIC_JMS_URL = "t3://172.17.0.2:7001"; // Weblogic JMS URL
    private String WEBLOGIC_JNDI_FACTORY_NAME = "weblogic.jndi.WLInitialContextFactory"; // Weblogic JNDI
    private String CONNECTION_FACTORY_JNDI_NAME = "jms/test/TestConnFactory"; // Weblogic ConnectionFactory JNDI
    private String QUEUE_JNDI_NAME = "jms/test/TestQueue"; // Weblogic Queue JNDI
 
    // variables 
    private Hashtable<String, String> env = null;
    private InitialContext initialContext = null;
    private QueueConnectionFactory queueConnectionFactory = null;
    private QueueConnection queueConnection = null;
    private QueueSession queueSession = null;
    private Queue queue = null;
    private QueueSender queueSender = null;
    private TextMessage textMessage = null;
 
    /**
     * default constructor
     */
    public JMSQueueProducer() {
 
        this.env = new Hashtable<String, String>();
        this.env.put(Context.PROVIDER_URL, WEBLOGIC_JMS_URL); // set Weblogic JMS URL
        this.env.put(Context.INITIAL_CONTEXT_FACTORY, WEBLOGIC_JNDI_FACTORY_NAME); // set Weblogic JNDI
        this.env.put(Context.SECURITY_PRINCIPAL, "weblogic"); // set Weblogic UserName
        this.env.put(Context.SECURITY_CREDENTIALS, "welcome1"); // set Weblogic PassWord
    }
 
    /**
     * This method initializes all necessary connection parameters for establishing JMS Queue communication
     * 
     * @throws NamingException
     * @throws JMSException
     */
    public void initializeConnParams() throws NamingException, JMSException {
 
        initialContext = new InitialContext(this.env); // set InitialContext 
        queueConnectionFactory = (QueueConnectionFactory) initialContext.lookup(CONNECTION_FACTORY_JNDI_NAME); // lookup using initial context
        queueConnection = queueConnectionFactory.createQueueConnection(); // create ConnectionFactory
        queueSession = queueConnection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE); // create QueueSession
        queue = (Queue) initialContext.lookup(QUEUE_JNDI_NAME); // lookup Queue JNDI using initial context created above
        queueSender = queueSession.createSender(queue); // create Sender using Queue JNDI as arguments
        textMessage = queueSession.createTextMessage(); // create TextMessage for Queue
        queueConnection.start(); // start Queue connection
    }
 
    /**
     * This is the actual method to SEND messages to JMS Queues
     * 
     * @param message
     * @throws JMSException
     */
    public void send(String message) throws JMSException {
 
        textMessage.setText(message);
        queueSender.send(textMessage);
    }
 
    /**
     * This method closes all connections
     * 
     * @throws JMSException
     */
    public void closeConnParams() throws JMSException {
 
        queueSender.close();
        queueSession.close();
        queueConnection.close();
    }
 
        // create an object to invoke initialize(), send() and close() methods
        // JMSQueueProducer jms = new JMSQueueProducer();
        // jms.initializeConnParams(); // invokes initialize method to all necessary connections
        // jms.send("JMS Queue testing"); // invokes send method with actual message
        // jms.closeConnParams(); // invokes to close all connections
        // javac -cp ./com.ibm.mq.allclient-9.2.4.0.jar:./javax.jms-api-2.0.1.jar:./java-json.jar JMSQueueProducer.java
        
}
